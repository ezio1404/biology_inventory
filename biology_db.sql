-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2018 at 07:31 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biology_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `displaytbl_slipLog` ()  SELECT * FROM tbl_sliplog$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_apparatus`
--

CREATE TABLE `tbl_apparatus` (
  `apparatus_id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `apparatus_name` varchar(255) NOT NULL,
  `apparatus_desc` varchar(255) NOT NULL,
  `apparatus_status` varchar(255) NOT NULL DEFAULT 'Available'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_apparatus`
--

INSERT INTO `tbl_apparatus` (`apparatus_id`, `sup_id`, `apparatus_name`, `apparatus_desc`, `apparatus_status`) VALUES
(1, 1, 'test', 'testtest', 'test'),
(5, 2, 'tes', 'is hist', 'Available');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slip`
--

CREATE TABLE `tbl_slip` (
  `slip_id` int(11) NOT NULL,
  `stud_id` int(11) NOT NULL,
  `apparatus_id` int(11) NOT NULL,
  `slip_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slip`
--

INSERT INTO `tbl_slip` (`slip_id`, `stud_id`, `apparatus_id`, `slip_status`) VALUES
(3, 15387467, 1, 1),
(4, 0, 1, 1),
(5, 0, 1, 1),
(6, 15387467, 5, 1),
(7, 15387467, 5, 1);

--
-- Triggers `tbl_slip`
--
DELIMITER $$
CREATE TRIGGER `display_tbl_slipLog` AFTER INSERT ON `tbl_slip` FOR EACH ROW insert into tbl_sliplog(slip_id) values(NEW.slip_id)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sliplog`
--

CREATE TABLE `tbl_sliplog` (
  `log_id` int(11) NOT NULL,
  `slip_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sliplog`
--

INSERT INTO `tbl_sliplog` (`log_id`, `slip_id`, `date`) VALUES
(2, 3, '2018-10-13 15:25:20'),
(3, 4, '2018-10-13 17:17:29'),
(4, 5, '2018-10-13 17:17:30'),
(5, 6, '2018-10-13 17:17:36'),
(6, 7, '2018-10-13 17:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `stud_id` int(11) NOT NULL,
  `stud_fname` varchar(255) NOT NULL,
  `stud_lname` varchar(255) NOT NULL,
  `stud_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`stud_id`, `stud_fname`, `stud_lname`, `stud_group`) VALUES
(0, 'test', 'aet', 1),
(15387467, 'ej anton', 'potot', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `sup_id` int(11) NOT NULL,
  `sup_name` varchar(255) NOT NULL,
  `sup_email` varchar(255) NOT NULL,
  `sup_address` varchar(255) NOT NULL,
  `sup_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`sup_id`, `sup_name`, `sup_email`, `sup_address`, `sup_status`) VALUES
(1, 'test', 'testtest', 'testtest', 1),
(2, 'maersk', 'maer@maerk', '1asd Df', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_workingstudent`
--

CREATE TABLE `tbl_workingstudent` (
  `ws_id` int(11) NOT NULL,
  `ws_fname` varchar(255) NOT NULL,
  `ws_lname` varchar(255) NOT NULL,
  `ws_password` varchar(255) NOT NULL,
  `ws_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_workingstudent`
--

INSERT INTO `tbl_workingstudent` (`ws_id`, `ws_fname`, `ws_lname`, `ws_password`, `ws_status`) VALUES
(123, 'test', 'test', 'test', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_apparatus`
--
ALTER TABLE `tbl_apparatus`
  ADD PRIMARY KEY (`apparatus_id`),
  ADD KEY `sup_id` (`sup_id`);

--
-- Indexes for table `tbl_slip`
--
ALTER TABLE `tbl_slip`
  ADD PRIMARY KEY (`slip_id`),
  ADD KEY `apparatus_id` (`apparatus_id`),
  ADD KEY `stud_id` (`stud_id`);

--
-- Indexes for table `tbl_sliplog`
--
ALTER TABLE `tbl_sliplog`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`stud_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `tbl_workingstudent`
--
ALTER TABLE `tbl_workingstudent`
  ADD PRIMARY KEY (`ws_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_apparatus`
--
ALTER TABLE `tbl_apparatus`
  MODIFY `apparatus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_slip`
--
ALTER TABLE `tbl_slip`
  MODIFY `slip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_sliplog`
--
ALTER TABLE `tbl_sliplog`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `sup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_apparatus`
--
ALTER TABLE `tbl_apparatus`
  ADD CONSTRAINT `tbl_apparatus_ibfk_1` FOREIGN KEY (`sup_id`) REFERENCES `tbl_supplier` (`sup_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_slip`
--
ALTER TABLE `tbl_slip`
  ADD CONSTRAINT `tbl_slip_ibfk_1` FOREIGN KEY (`apparatus_id`) REFERENCES `tbl_apparatus` (`apparatus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_slip_ibfk_2` FOREIGN KEY (`stud_id`) REFERENCES `tbl_student` (`stud_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
