<?php
require_once '../model/apparatusModel.php';
$Apparatus = new Apparatus();
    if(isset($_POST['addAppa'])){
    $flag=true;
    $appa_name=htmlentities($_POST['appa_name']);
    $appa_desc=htmlentities($_POST['appa_desc']);
    $supplier_id=htmlentities($_POST['supplier_id']);
    $appaArray=array($supplier_id,$appa_name,$appa_desc);
    for($i=0;$i<count($appaArray);$i++){
        if($appaArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $Apparatus->addApparatus($appaArray);
        header('location:../view/appa.php');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}