<?php
require_once '../model/slipModel.php';
$Slip = new Slip();
    if(isset($_POST['addSlip'])){
    $flag=true;
    $stud_id=htmlentities($_POST['stud_id']);
    $apparatus_id=htmlentities($_POST['appa_id']);
    $slipArray=array($stud_id,$apparatus_id);
    for($i=0;$i<count($slipArray);$i++){
        if($slipArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $Slip->addSlip($slipArray);
        header('location:../view/slip.php');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}