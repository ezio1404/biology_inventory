<?php
require_once '../model/StudentModel.php';
$Student = new Student();
    if(isset($_POST['addStud'])){
    $flag=true;
    $stud_id=htmlentities($_POST['stud_id']);
    $stud_fname=htmlentities($_POST['stud_fname']);
    $stud_lname=htmlentities($_POST['stud_lname']);
    $stud_group=htmlentities($_POST['stud_group']);
    $studArray=array($stud_id,$stud_fname,$stud_lname,$stud_group);
    for($i=0;$i<count($studArray);$i++){
        if($studArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $Student->addStudent($studArray);
        header('location:../view/stud.php');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}