<?php
require_once '../model/supplierModel.php';
$supplier = new Supplier();
    if(isset($_POST['addSup'])){
    $flag=true;
    $name=htmlentities($_POST['sup_name']);
    $maxseats=htmlentities($_POST['sup_email']);
    $status=htmlentities($_POST['sup_addr']);
    $supArray=array($name,$maxseats,$status);
    for($i=0;$i<count($supArray);$i++){
        if($supArray[$i]==""){
            $flag=false;
            break;
        }
    }

    if($flag){
        $supplier->addSupplier($supArray);
        header('location:../view/sup.php');
     }
     else{
        $message = "Error";
        echo "<script type='text/javascript'>alert('$message');</script>";  
     }
}