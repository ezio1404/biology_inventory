<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVENTORY</title>
    <link rel="stylesheet" href="view/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/assets/css/signin.css">
    <link rel="stylesheet" href="view/assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="view/assets/css/jquery.dataTables.min.css">
</head>
<body>
<form class="form-signin" method="POST" action="controller/wsLog.php">
      <img class="mb-4" src="view/assets/images/icon.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Admin Login</h1>
      <label for="email" class="sr-only">Username</label>
      <input type="text" id="email" name="email" class="form-control" placeholder="Username" required autofocus>
      <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" name="password"  class="form-control" placeholder="Password" required>
        
      <input class="btn btn-lg btn-primary btn-block" type="submit" name="loginAdmin">
      <p class="mt-5 mb-3 text-muted">&copy; Biology 2017-2018</p>
    </form>
</body>
<script src="view/assets/js/jquery-3.3.1.js"></script>
<script src="view/assets/js/jquery.dataTables.min.js"></script>
<script src="view/assets/js/dataTables.buttons.min.js"></script>
<script src="view/assets/js/buttons.print.min.js"></script>
<script src="view/assets/js/buttons.flash.min.js"></script>
<script src="view/assets/js/buttons.html5.min.js"></script>
<script src="view/assets/js/jszip.min.js"></script>
<script src="view/assets/js/pdfmake.min.js"></script>
<script src="view/assets/js/vfs_fonts.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
</script>
<script src="view/assets/js/bootstrap.js"></script>
</html>