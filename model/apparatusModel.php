<?php
require_once 'db/dbhelper.php';
Class Apparatus extends DBHelper{
    private $table = 'tbl_apparatus';
    private $fields = array(
        'sup_id',
        'apparatus_name',
        'apparatus_desc',
    );
    private $Updatefields = array(
        'sup_id',
        'apparatus_name',
        'apparatus_desc',
        'apparatus_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addApparatus($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllApparatus(){
     return DBHelper::getAllRecord($this->table);
 }
 function getApparatusById($ref_id){
    return DBHelper::getRecordById($this->table,'apparatus_id',$ref_id);
}
function getApparatus($ref_id){
    return DBHelper::getRecord($this->table,'apparatus_id',$ref_id);
}
// Update
function updateApparatus($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'apparatus_id',$ref_id); 
 }
 // Delete
 function deleteApparatus($ref_id){
          return DBHelper::deleteRecord($this->table,'apparatus_id',$ref_id);
}
// Some Functions
    function getCountApparatus(){
        return DBHelper::countRecord('apparatus_id',$this->table);
    }
}
?>