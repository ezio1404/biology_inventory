<?php
require_once 'db/dbhelper.php';
Class SlipLog extends DBHelper{
    private $table = 'tbl_slipLog';

//constructor
    function __construct(){
        return DBHelper::__construct();
    }
    function getProcedureLog()
    {
        return DBHelper::getProcedure($this->table);
    }
}
?>