<?php
require_once 'db/dbhelper.php';
Class Slip extends DBHelper{
    private $table = 'tbl_slip';
    private $fields = array(
        'stud_id',
        'apparatus_id'
    );
    private $Updatefields = array(
        'stud_id',
        'apparatus_id',
        'slip_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addSlip($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllSlip(){
     return DBHelper::getAllRecord($this->table);
 }
 function getSlipById($ref_id){
    return DBHelper::getRecordById($this->table,'slip_id',$ref_id);
}
function getSlip($ref_id){
    return DBHelper::getRecord($this->table,'slip_id',$ref_id);
}
// Update
function updateSlip($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'slip_id',$ref_id); 
 }
 // Delete
 function deleteSlip($ref_id){
          return DBHelper::deleteRecord($this->table,'slip_id',$ref_id);
}
// Some Functions
    function getCountSlip(){
        return DBHelper::countRecord('slip_id',$this->table);
    }
}
?>