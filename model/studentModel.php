<?php
require_once 'db/dbhelper.php';
Class Student extends DBHelper{
    private $table = 'tbl_student';
    private $fields = array(
        'stud_id',
        'stud_fname',
        'stud_lname',
        'stud_group'
    );
    private $Updatefields = array(
        'stud_id',
        'stud_fname',
        'stud_lname',
        'stud_group'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addStudent($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllStudent(){
     return DBHelper::getAllRecord($this->table);
 }
 function getStudentById($ref_id){
    return DBHelper::getRecordById($this->table,'stud_id',$ref_id);
}
function getStudent($ref_id){
    return DBHelper::getRecord($this->table,'stud_id',$ref_id);
}
// Update
function updateStudent($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'stud_id',$ref_id); 
 }
 // Delete
 function deleteStudent($ref_id){
          return DBHelper::deleteRecord($this->table,'stud_id',$ref_id);
}
// Some Functions
    function getCountStudent(){
        return DBHelper::countRecord('stud_id',$this->table);
    }
}
?>