<?php
require_once 'db/dbhelper.php';
Class Supplier extends DBHelper{
    private $table = 'tbl_supplier';
    private $fields = array(
        'sup_name',
        'sup_email',
        'sup_address'
    );
    private $Updatefields = array(
        'sup_name',
        'sup_email',
        'sup_address',
        'sup_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addSupplier($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllSupplier(){
     return DBHelper::getAllRecord($this->table);
 }
 function getSupplierById($ref_id){
    return DBHelper::getRecordById($this->table,'sup_id',$ref_id);
}
function getSupplier($ref_id){
    return DBHelper::getRecord($this->table,'sup_id',$ref_id);
}
// Update
function updateSupplier($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'sup_id',$ref_id); 
 }
 // Delete
 function deleteSupplier($ref_id){
          return DBHelper::deleteRecord($this->table,'sup_id',$ref_id);
}
// Some Functions
    function getCountSupplier(){
        return DBHelper::countRecord('sup_id',$this->table);
    }
}
?>