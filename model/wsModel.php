<?php
require_once 'db/dbhelper.php';
Class WS extends DBHelper{
    private $table = 'tbl_workingStudent';
    private $fields = array(
        'ws_id',
        'ws_fname',
        'ws_lname',
        'ws_password'        
    );
    private $Updatefields = array(
        'ws_id',
        'ws_fname',
        'ws_lname',
        'ws_password',
        'ws_status'
    );
//constructor
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addWS($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllWS(){
     return DBHelper::getAllRecord($this->table);
 }
 function getWSById($ref_id){
    return DBHelper::getRecordById($this->table,'ws_id',$ref_id);
}
function getWS($ref_id){
    return DBHelper::getRecord($this->table,'ws_id',$ref_id);
}
// Update
function updateWS($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->Updatefields,$data,'ws_id',$ref_id); 
 }
 // Delete
 function deleteWS($ref_id){
          return DBHelper::deleteRecord($this->table,'ws_id',$ref_id);
}
// Some Functions
    function getCountWS(){
        return DBHelper::countRecord('ws_id',$this->table);
    }
}
?>