<?php
require_once '../model/apparatusModel.php';
require_once '../model/supplierModel.php';

if($_SESSION){
    $apparatus=new Apparatus();
    $appa=$apparatus->getAllApparatus();
    $supplier = new Supplier();
    $sup=$supplier->getAllSupplier();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVENTORY</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
</head>
<body>
<?php require 'nav.php';?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <h1>Add Apparatus</h1>
            <form action="../controller/appaCtrl.php" method="POST">
                <input type="text" name="appa_name" id="appa_name" class="form-control" require placeholder="Apparatus Name">
                <input type="text" name="appa_desc" id="appa_desc" class="form-control" require placeholder="Apparatus Desc">
                <select  class="form-control" name="supplier_id" id="supplier_id">
                    <?php
                    if(count($sup)){
                        foreach($sup as $s){
                            ?>
                            <option value="<?php echo $s['sup_id']?>"><?php echo $s['sup_name'];?></option>
                            <?php
                        }
                    }else{
                        ?>
                        <option selected disabled>Empty</option>
                    <?php
                    }
                    ?>
                </select>
                <input type="submit" value="+ Apparatus" name="addAppa" class="btn btn-success">
            </form>
        </div>
        <div class="col-sm-6">
            <h1>Apparatus</h1>
        <table id="example" class="display nowrap" style="width:100%">
            <thead>
                <th>#</th>
                <th>Supplier ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Status</th>
        </thead>
        <tbody>
            <?php
                foreach($appa as $a){
            ?>
            <tr>
                <td><?php echo $a['apparatus_id']?></td>
                <td><?php echo $a['sup_id']?></td>
                <td><?php echo $a['apparatus_name']?></td>
                <td><?php echo $a['apparatus_desc']?></td>
                <td><?php echo $a['apparatus_status']?></td>
            </tr>
            <?php
                }
            ?>
        </tbody>
            </table>
        </div>
    </div>
</div>

</body>
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.buttons.min.js"></script>
<script src="assets/js/buttons.print.min.js"></script>
<script src="assets/js/buttons.flash.min.js"></script>
<script src="assets/js/buttons.html5.min.js"></script>
<script src="assets/js/jszip.min.js"></script>
<script src="assets/js/pdfmake.min.js"></script>
<script src="assets/js/vfs_fonts.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
</script>
<script src="assets/js/bootstrap.js"></script>
</html>
<?php
}else{
    header("Location:../index.php?Please_login");
}
?>