<?php
require_once '../model/slipLogModel.php';
if($_SESSION){

    $sliplog=new SlipLog();
    $sliplogs=$sliplog->getProcedureLog();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVENTORY</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
</head>
<body>
<?php require_once 'nav.php';?>

<div class="container-fluid">

  
            <h1>Slip Logs</h1>
        <table id="example" class="display nowrap" style="width:100%">
            <thead>
                <th>Log #</th>
                <th>Slip #</th>
                <th>Date</th>
        </thead>
        <tbody>
            <?php
                foreach($sliplogs as $sl){
            ?>
            <tr>
                <td><?php echo $sl['log_id']?></td>
                <td><?php echo $sl['slip_id']?></td>
                <td><?php echo $sl['date']?></td>
            </tr>
            <?php
                }
            ?>
        </tbody>
            </table>

</div>
</body>
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.buttons.min.js"></script>
<script src="assets/js/buttons.print.min.js"></script>
<script src="assets/js/buttons.flash.min.js"></script>
<script src="assets/js/buttons.html5.min.js"></script>
<script src="assets/js/jszip.min.js"></script>
<script src="assets/js/pdfmake.min.js"></script>
<script src="assets/js/vfs_fonts.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
</script>
<script src="assets/js/bootstrap.js"></script>
</html>
<?php
}else{
    header("Location:../index.php?Please_login");
}
?>