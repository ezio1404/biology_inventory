<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="home.php">Biology Inventory</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="home.php">Home </a>
      <a class="nav-item nav-link" href="sup.php">Supplier</a>
      <a class="nav-item nav-link" href="appa.php">Apparatus</a>
      <a class="nav-item nav-link" href="stud.php">Student</a>
      <a class="nav-item nav-link" href="slip.php">Slip</a>
      <form  action="../controller/wsLog.php" method="post">
              <input class="btn btn-outline-danger" type="submit" value="Logout" name=logoutAdmin>
      </form>
    </div>
  </div>
</nav>