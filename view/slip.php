<?php
require_once '../model/apparatusModel.php';
require_once '../model/studentModel.php';
require_once '../model/slipModel.php';
if($_SESSION){
    $apparatus = new Apparatus();
    $Student = new Student();
    $Slip = new Slip();
    $slipList= $Slip->getAllSlip();
    $appaList= $apparatus->getAllApparatus();
    $studentList= $Student->getAllStudent();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVENTORY</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
</head>
<body>
<?php require_once 'nav.php';?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <h1>Add Slip</h1>
            <form action="../controller/slipCtrl.php" method="POST">
            <label for="appa_id">Apparatus :</label>
            <select  class="form-control" name="appa_id" id="appa_id">
                    <?php
                    if(count($appaList)){
                        foreach($appaList as $appa){
                            ?>
                            <option value="<?php echo $appa['apparatus_id']?>"><?php echo $appa['apparatus_name'];?></option>
                            <?php
                        }
                    }else{
                        ?>
                        <option selected disabled>Empty</option>
                    <?php
                    }
                    ?>
                </select>
                <label for="stud_id">Student :</label>
                <select  class="form-control" name="stud_id" id="stud_id">
                    <?php
                    if(count($studentList)){
                        foreach($studentList as $stud){
                            ?>
                            <option value="<?php echo $stud['stud_id']?>"><?php echo $stud['stud_lname'].','.$stud['stud_fname'];?></option>
                            <?php
                        }
                    }else{
                        ?>
                        <option selected disabled>Empty</option>
                    <?php
                    }
                    ?>
                </select>
                <input type="submit" value="+ Slip" class="btn btn-success" name="addSlip">
        </form>
        </div>
        <div class="col-sm-6">
            <h1>Slip</h1>
        <table id="example" class="display nowrap" style="width:100%">
            <thead>
                <th>#</th>
                <th>Stud #</th>
                <th>Apparatus #</th>
                <th>Status</th>
        </thead>
        <tbody>
            <?php
                foreach($slipList as $slip){
            ?>
            <tr>
                <td><?php echo $slip['slip_id']?></td>
                <td><?php echo $slip['stud_id']?></td>
                <td><?php echo $slip['apparatus_id']?></td>
                <td><?php echo $slip['slip_status']?></td>
            </tr>
            <?php
                }
            ?>
        </tbody>
            </table>
        </div>
    </div>
</div>


</body>
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.buttons.min.js"></script>
<script src="assets/js/buttons.print.min.js"></script>
<script src="assets/js/buttons.flash.min.js"></script>
<script src="assets/js/buttons.html5.min.js"></script>
<script src="assets/js/jszip.min.js"></script>
<script src="assets/js/pdfmake.min.js"></script>
<script src="assets/js/vfs_fonts.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
</script>
<script src="assets/js/bootstrap.js"></script>
</html>
<?php
}else{
    header("Location:../index.php?Please_login");
}
?>