<?php
require_once '../model/studentModel.php';

if($_SESSION){
    $Student=new Student();
    $studs=$Student->getAllStudent();
    ?>
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVENTORY</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
</head>
<body>
<?php require_once 'nav.php';?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <h1>Add Student</h1>
            <form action="../controller/studCtrl.php" method="POST">
                <input type="number" name="stud_id" id="stud_id" class="form-control" value='00000000' >
                <input type="text" name="stud_fname" id="stud_fname" class="form-control" require placeholder="Student Firstname">
                <input type="text" name="stud_lname" id="stud_lname" class="form-control" require placeholder="Student lastname">
                <input type="number" name="stud_group" id="stud_group" class="form-control" value=1 min=1 max=10>
                <input type="submit" value="+ Student" name="addStud" class="btn btn-success">
            </form>
        </div>
        <div class="col-sm-6">
            <h1>Student</h1>
        <table id="example" class="display nowrap" style="width:100%">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Group #</th>
        </thead>
        <tbody>
            <?php
                foreach($studs as $stud){
            ?>
            <tr>
                <td><?php echo $stud['stud_id']?></td>
                <td><?php echo $stud['stud_lname'].','.$stud['stud_fname']?></td>
                <td><?php echo $stud['stud_group']?></td>
            </tr>
            <?php
                }
            ?>
        </tbody>
            </table>
        </div>
    </div>
</div>

</body>
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.buttons.min.js"></script>
<script src="assets/js/buttons.print.min.js"></script>
<script src="assets/js/buttons.flash.min.js"></script>
<script src="assets/js/buttons.html5.min.js"></script>
<script src="assets/js/jszip.min.js"></script>
<script src="assets/js/pdfmake.min.js"></script>
<script src="assets/js/vfs_fonts.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
</script>
<script src="assets/js/bootstrap.js"></script>
</html>
<?php
}else{
    header("Location:../index.php?Please_login");
}
?>