<?php
require '../model/supplierModel.php';

if($_SESSION){
$supplier= new Supplier();
$sup=$supplier->getAllSupplier();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVENTORY</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">

</head>
<body>
<?php require_once 'nav.php';?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <h1>Add Supplier</h1>
            <form action="../controller/supCtrl.php" method="POST">
                <input type="text" name="sup_name" id="sup_name" class="form-control" placeholder="Supplier Name"required>
                <input type="email" name="sup_email" id="sup_email" class="form-control" placeholder="Supplier Email"required>
                <input type="text" name="sup_addr" id="sup_addr" class="form-control" placeholder="Supplier Address"required>
                <div>
                <input type="submit" value="+ Supplier" name="addSup" class="btn btn-success">
            </div>
            </form>
        </div>
        <div class="col-sm-6">
            <h1>Supplier List</h1>
            <table id="example" class="display nowrap" style="width:100%">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Status</th>
     
        </thead>
        <tbody>
            <?php
                foreach($sup as $s){
            ?>
            <tr>
                <td><?php echo $s['sup_id']?></td>
                <td><?php echo $s['sup_name']?></td>
                <td><?php echo $s['sup_email']?></td>
                <td><?php echo $s['sup_address']?></td>
                <td><?php echo $s['sup_status']?></td>
            </tr>
            <?php
                }
            ?>
        </tbody>
            </table>
        </div>
    </div>
</div>


</body>
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.buttons.min.js"></script>
<script src="assets/js/buttons.print.min.js"></script>
<script src="assets/js/buttons.flash.min.js"></script>
<script src="assets/js/buttons.html5.min.js"></script>
<script src="assets/js/jszip.min.js"></script>
<script src="assets/js/pdfmake.min.js"></script>
<script src="assets/js/vfs_fonts.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
</script>
<script src="assets/js/bootstrap.js"></script>
</html>
<?php
}else{
    header("Location:../index.php?Please_login");
}
?>